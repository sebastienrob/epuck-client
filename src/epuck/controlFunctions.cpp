#include <controlFunctions.h>

/**** Derive robot pose variation from encoders and save it along with robot linear and angular velocities ****/
RobPose GetCurrPoseFromEncoders(const RobPose & prevRobPose, const int& EncL, const int& EncR, const int& prevEncL, const int& prevEncR, const string & data_folder){
    printf("Left Encoder : %i ", EncL);
    printf("Right Encoder : %i \n", EncR);
    const float encoderFactor = 0.052;
    const float wheel_radius = 0.0205;//TODO replace by rp.wheel_radius when RobotParameters will be here
    const float axisLength = 0.026;//TODO replace by rp.wheel_radius when RobotParameters will be here
    const float sampleTime = 0.1;

    float linVel, angVel, dx, dy, dTh;
    float dEncL = EncL - prevEncL;
    float dEncR = EncR - prevEncR;
    linVel = wheel_radius * encoderFactor * (dEncR + dEncL) / 2;
    angVel = wheel_radius * encoderFactor * (dEncR - dEncL) /
            (2 * axisLength);
    dTh = angVel * sampleTime;
    dx = linVel * cos(prevRobPose.getTh() +dTh);
    dy = linVel * sin(prevRobPose.getTh() +dTh);
    RobPose curRobPose(prevRobPose.getX()+dx, prevRobPose.getY()+dy, prevRobPose.getTh() +dTh);

    //log the data
    FILE *v, *w, *Dx, *Dy, *Dth, *x, *y, *th, *time;
    v = fopen((data_folder + "v.txt").c_str(), "a+");
    w = fopen((data_folder + "w.txt").c_str(), "a+");
    Dx = fopen((data_folder + "dx.txt").c_str(), "a+");
    Dy = fopen((data_folder + "dy.txt").c_str(), "a+");
    Dth = fopen((data_folder + "dTheta.txt").c_str(), "a+");
    x = fopen((data_folder + "x.txt").c_str(), "a+");
    y = fopen((data_folder + "y.txt").c_str(), "a+");
    th= fopen((data_folder + "theta.txt").c_str(), "a+");
    time = fopen((data_folder + "time.txt").c_str(), "a+");
    fprintf(v, "%f\n", linVel);
    fprintf(w, "%f\n", angVel);
    fprintf(Dx, "%f\n", dx);
    fprintf(Dy, "%f\n", dy);
    fprintf(Dth, "%f\n", dTh);
    fprintf(x, "%f\n", curRobPose.getX());
    fprintf(y, "%f\n", curRobPose.getY());
    fprintf(th, "%f\n", curRobPose.getTh());
    fprintf(time, "%f\n", sampleTime);
    fclose(Dx);
    fclose(Dy);
    fclose(Dth);
    fclose(v);
    fclose(w);
    fclose(x);
    fclose(y);
    fclose(th);
    fclose(time);
    return(curRobPose);
}
/**** Show image from the robot camera and save it on the disk ****/
Mat LoadAndShowImageFromDisk(const string folderName, const int& cnt_it) {
    //load the image from the disk
    char imgNameIteration[30];
    sprintf(imgNameIteration, "images/image%04d.jpg", cnt_it);
    Mat colorRawImg = imread(folderName + imgNameIteration);
    cout << "\nLoadImage loaded image " << folderName + imgNameIteration << endl;
    namedWindow( "loggedImg", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "loggedImg", 0,0); // Move window
    imshow( "loggedImg", colorRawImg);                // Show the image inside it
    return(colorRawImg);
}
/**** Load encoder measurements from logs on disk ****/
vector<int> LoadEncoderLogs(const string folderName) {
    vector<int> encData;
    //load the data from the disk
    FILE *enc;
    enc = fopen(folderName.c_str(), "r");
    cout << "Loading encoder from " << folderName << endl;
    int i=0;
    int tmp;
    while(fscanf(enc,"%d",&tmp)!=EOF) {
        encData.push_back(tmp);
        printf("i %d\t enc %d\n", i, encData[i]);
        i++;
    };
    int size = i;
    cout << "size " << size << endl;
    return(encData);
}
void DrawMapWithRobot(const RobPose & rPoseEnc, const RobPose & rPoseVis){
    Mat mapImg;
    static const int cmInPixels = 15;
    static const int xMaxInCm = 60;
    static const int yMaxInCm = 40;
    static const int xOriginInCm = 5;
    //draw empty map
    mapImg.create((xMaxInCm + xOriginInCm)*cmInPixels, yMaxInCm*cmInPixels, CV_8UC3);
    mapImg.setTo(Scalar(255,255,255));

    //draw scale
    line(mapImg, Point(xOriginInCm*cmInPixels,xOriginInCm*cmInPixels), Point(xOriginInCm*cmInPixels,(xOriginInCm + 1)*cmInPixels), Scalar(0, 100, 0),2);
    string cmText = "1 cm";
    putText(mapImg, cmText, Point(xOriginInCm*cmInPixels+3,(xOriginInCm+0.5)*cmInPixels), FONT_HERSHEY_PLAIN, 1.0, Scalar(0, 100, 0),2);

    //draw x axis
    arrowedLine(mapImg, Point((yMaxInCm/2)*cmInPixels,xOriginInCm*cmInPixels), Point((yMaxInCm/2)*cmInPixels,(xOriginInCm+xMaxInCm/3)*cmInPixels), Scalar(255, 0, 0),2,8,0,0.03);
    string xText = "x";
    putText(mapImg, xText, Point((yMaxInCm/2-1)*cmInPixels,(xOriginInCm+1)*cmInPixels), FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 0, 0),2);

    //draw y axis
    arrowedLine(mapImg, Point((yMaxInCm/2)*cmInPixels,xOriginInCm*cmInPixels), Point((yMaxInCm/2+yMaxInCm/3)*cmInPixels,xOriginInCm*cmInPixels), Scalar(255, 0, 0),2,8,0,0.03);
    string yText = "y";
    putText(mapImg, yText, Point((yMaxInCm/2+1)*cmInPixels,(xOriginInCm-1)*cmInPixels), FONT_HERSHEY_PLAIN, 1.0, Scalar(255, 0, 0),2);

    float robRadius = 3.5*cmInPixels;
    //draw robot pose from encoders
    float xEnc = rPoseEnc.getX();
    float yEnc = rPoseEnc.getY();
    float thEnc  = rPoseEnc.getTh();
    float uEnc = (yEnc*100 + yMaxInCm/2)*cmInPixels;
    float vEnc = (xEnc*100 + xOriginInCm)*cmInPixels;
    line(mapImg, Point(uEnc,vEnc), Point(uEnc-robRadius*sin(thEnc-M_PI),vEnc-robRadius*cos(thEnc-M_PI)), Scalar(100, 0, 100),2);
    circle(mapImg, Point(uEnc,vEnc), robRadius, Scalar(100, 0, 100), 2);

    //draw robot pose from vision
    float xVis = rPoseVis.getX();
    float yVis = rPoseVis.getY();
    float thVis  = rPoseVis.getTh();
    float uVis = (yVis*100 + yMaxInCm/2)*cmInPixels;
    float vVis = (xVis*100 + xOriginInCm)*cmInPixels;
    line(mapImg, Point(uVis,vVis), Point(uVis-robRadius*sin(thVis-M_PI),vVis-robRadius*cos(thVis-M_PI)), Scalar(0, 0, 255),2);
    circle(mapImg, Point(uVis,vVis), robRadius, Scalar(0, 0, 255), 2);

    //show map
    namedWindow( "map", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "map", 1020,0); // Move window
    imshow( "map", mapImg);                // Show the image inside it
}
Point ProcessImageToGetBarycenter(const Mat& colRawImg) {
    struct timeval start, end;
    gettimeofday(&start, NULL); //get start time of function
    Mat greyImg;
    cvtColor(colRawImg, greyImg, cv::COLOR_BGR2GRAY); //convert image to black and white
    namedWindow( "greyImg", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "greyImg", 340,0); // Move window
    imshow( "greyImg", greyImg);                // Show the image inside it

    //TODO process images to get barycenter
    Mat processedImg;
    cvtColor(greyImg, processedImg, cv::COLOR_GRAY2BGR);//convert image to color (for displaying colored features)
    Point targetBarycenter;
    targetBarycenter.x = 160;//default coordinates - to be changed
    targetBarycenter.y = 120;
    cout << "targetBarycenter is at " << targetBarycenter << endl; //print coordinates
    circle(processedImg, targetBarycenter, 3, Scalar(0, 0, 255), 1); 	//display red circle
    namedWindow( "processedImg", WINDOW_AUTOSIZE ); // Create a window for display
    moveWindow( "processedImg", 680,0); // Move window
    imshow( "processedImg", processedImg);                // Show the image inside it

    gettimeofday(&end, NULL); //get end time
    double time_spent = (end.tv_sec - start.tv_sec) * 1e3 + (end.tv_usec -
                                                             start.tv_usec) * 1e-3;
    printf("ProcessImageToGetBarycenter took %f ms\n", time_spent); //print time spetn for processing
    return(targetBarycenter);
}
/**** Send commands (speed_L, speed_R) to motors in order to realize robot
 * velocities (v, w) for 10 seconds ****/
void SetRobotVelocities(struct timeval startTime, const float& vel,
                        const float& omega, char MotorCmd[15]) {
    printf("The required robot velocities are v %f w %f\n\n\n", vel, omega);
    struct timeval curTime;
    gettimeofday(&curTime, NULL);
    long int timeSinceStart =
            ((curTime.tv_sec * 1000000 + curTime.tv_usec) -
             (startTime.tv_sec * 1000000 + startTime.tv_usec)) /
            1000;
    printf("timeSinceStart = %ld ms\n", timeSinceStart);
    if (timeSinceStart < 10000) {
        // write here the equations to convert (vel, omega) into (speed_L,
        // speed_R)
        int speed_L = 0;
        int speed_R = 0;
        // Sends the command to the epuck motors
        sprintf(MotorCmd, "D,%d,%d", speed_L, speed_R);
    } else {
        sprintf(MotorCmd, "D,%d,%d", 0, 0);
    }
}

// control robot using images from the camera
void ControlRobotWithVisualServoing(struct timeval startTime,
                                    char MotorCmd[15]) {
}
// make robot follow a wall using infrared measurements for ten seconds
void ControlRobotToFollowWall(struct timeval startTime, char MotorCmd[15]) {
    // replace the lines below with commands v and w needed to follow wall
    // from xA, yA, xB et yB
    //TODO Load these variables from files xA.txt etc...
    //printf("infrared measures xA %f yA %f xB %f yB %f \n", xA, yA, xB, yB);
    // calculate equation of the line in the robot frame
    float mMur, pMur; // parametres definissant le mur dans le repere robot
    printf("equation of the line in the robot frame: y = %f x + %f \n", mMur,
           pMur);
    float v = 0;
    float w = 0;
    // Commands to be sent to the left and right wheels
    int speedL;
    int speedR;
    float tau = 0.0054;
    // measure the time
    struct timeval curTime;
    gettimeofday(&curTime, NULL);
    long int timeSinceStart =
            ((curTime.tv_sec * 1000000 + curTime.tv_usec) -
             (startTime.tv_sec * 1000000 + startTime.tv_usec)) /
            1000;
    printf("timeSinceStart = %ld ms\n", timeSinceStart);
    /*TODO uncomment this and fix below
    if (timeSinceStart < 10000) {
        speedL = (v - rp.axisLength * w) / (rp.wheel_radius * tau);
        speedR = (v + rp.axisLength * w) / (rp.wheel_radius * tau);
    } else {
        stop_threads = true;
    }*/
    printf("send to Left %d and to Right %d\n", speedL, speedR);
    sprintf(MotorCmd, "D,%d,%d", speedL, speedR);
}

